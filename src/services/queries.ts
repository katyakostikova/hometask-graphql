import {gql} from '@apollo/client';

const CURRENT_USER = gql`
  query {
    details {
      id
    }
  }
`;

const LOGIN = gql`
  mutation Login($email: String!, $password: String!) {
    login(input: {email: $email, password: $password}) {
      id
      token
      email
      name
    }
  }
`;

const REGISTER = gql`
  mutation Register(
    $email: String!
    $name: String!
    $phoneNumber: String!
    $password: String!
  ) {
    register(
      input: {
        email: $email
        name: $name
        phoneNumber: $phoneNumber
        password: $password
      }
    ) {
      email
      name
    }
  }
`;

const GET_PRODUCTS = gql`
  query Products($first: Int, $after: String, $filter: String) {
    products(
      first: $first
      after: $after
      where: {title: {contains: $filter}}
    ) {
      nodes {
        id
        title
        price
        description
        pictures {
          url
        }
      }
      pageInfo {
        endCursor
      }
    }
  }
`;

const GET_PRODUCT_BY_ID = gql`
  query ProductById($id: Int!) {
    product(id: $id) {
      title
      price
      description
      pictures {
        id
        url
      }
      seller {
        name
        email
        phoneNumber
        avatar
      }
    }
  }
`;

const ADD_PRODUCT = gql`
  mutation NewProduct(
    $title: String!
    $price: Decimal!
    $description: String!
  ) {
    createProduct(
      input: {title: $title, price: $price, description: $description}
    ) {
      id
      title
    }
  }
`;

const ADD_PRODUCT_PICTURE = gql`
  mutation NewPicture($id: Int!, $fileName: String!, $content: String!) {
    createPicture(
      productId: $id
      file: {fileName: $fileName, content: $content}
    ) {
      id
      url
    }
  }
`;

const DELETE_PRODUCT = gql`
  mutation DeleteProduct($id: Int!) {
    deleteProduct(id: $id)
  }
`;

const DELETE_PRODUCT_PICTURE = gql`
  mutation DeleteProductPicture($id: Int!, $productId: Int!) {
    deletePicture(id: $id, productId: $productId)
  }
`;

export {
  CURRENT_USER,
  LOGIN,
  REGISTER,
  GET_PRODUCTS,
  GET_PRODUCT_BY_ID,
  ADD_PRODUCT,
  ADD_PRODUCT_PICTURE,
  DELETE_PRODUCT,
  DELETE_PRODUCT_PICTURE,
};
