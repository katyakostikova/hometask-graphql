import {StyleSheet} from 'react-native';

const styles = StyleSheet.create({
  screen: {
    flex: 1,
  },
  productContent: {
    flexGrow: 1,
  },
  deleteButton: {
    width: '40%',
    alignSelf: 'center',
    marginBottom: 10,
  },
});

export {styles};
