import {useMutation, useQuery} from '@apollo/client';
import {RouteProp, useNavigation, useRoute} from '@react-navigation/native';
import React, {useContext, useEffect} from 'react';
import {Button, ScrollView, View} from 'react-native';
import {
  MarketListScreenProp,
  ProductDetailsScreenRouteProp,
} from '../../common/types/navigation';
import Header from '../../components/product-details/header/Header';
import PicturesGallery from '../../components/product-details/pictures-gallery/PicturesGallery';
import ProductInfo from '../../components/product-details/product-info/ProductInfo';
import SellerInfo from '../../components/product-details/seller/SellerInfo';
import {AppColors} from '../../config/app-colors';
import {
  DELETE_PRODUCT,
  DELETE_PRODUCT_PICTURE,
  GET_PRODUCT_BY_ID,
} from '../../services/queries';
import {UserContext} from '../../store/context';
import Preloader from '../preloader/Preloader';
import {styles} from './styles';

const ProductDetailsScreen = () => {
  const {productId} =
    useRoute<RouteProp<ProductDetailsScreenRouteProp>>().params;
  const navigation = useNavigation<MarketListScreenProp>();
  const {loading, data} = useQuery(GET_PRODUCT_BY_ID, {
    variables: {id: productId},
  });
  const {currentUser} = useContext(UserContext);
  const [deleteProduct, {data: delData, loading: DelLoading}] =
    useMutation(DELETE_PRODUCT);
  const [deletePicture] = useMutation(DELETE_PRODUCT_PICTURE);

  const handleDeleteProduct = () => {
    for (let i = 0; i < data.product.pictures.length; i++) {
      deletePicture({
        variables: {id: data.product.pictures[i].id, productId},
        context: {
          headers: {
            authorization: `Bearer ${currentUser?.token}`,
          },
        },
      });
    }
    deleteProduct({
      variables: {id: productId},
      context: {
        headers: {
          authorization: `Bearer ${currentUser?.token}`,
        },
      },
    });
    navigation.goBack();
  };

  useEffect(() => {
    if (delData) {
      navigation.navigate('MarketList');
    }
  }, [delData, navigation]);

  if (loading || DelLoading) {
    return <Preloader />;
  }

  return (
    <View style={styles.screen}>
      <Header />
      <ScrollView
        contentContainerStyle={styles.productContent}
        showsVerticalScrollIndicator={false}>
        <PicturesGallery pictures={data.product.pictures} />
        {currentUser?.email === data.product.seller.email && (
          <View style={styles.deleteButton}>
            <Button
              title="Delete product"
              onPress={handleDeleteProduct}
              color={AppColors.ERROR_TEXT}
            />
          </View>
        )}
        <ProductInfo
          title={data.product.title}
          price={data.product.price}
          description={data.product.description}
        />
      </ScrollView>
      <SellerInfo seller={data.product.seller} />
    </View>
  );
};

export default ProductDetailsScreen;
