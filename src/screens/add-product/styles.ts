import {StyleSheet} from 'react-native';

const styles = StyleSheet.create({
  screen: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  formContainer: {
    width: '75%',
    borderRadius: 50,
    justifyContent: 'space-evenly',
    alignItems: 'center',
  },
  button: {
    width: 70,
    height: 50,
  },
  buttonContainer: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    width: '60%',
    marginTop: 30,
  },
  imageScrollContainer: {
    height: '10%',
    width: '90%',
  },
  removeIcon: {
    position: 'absolute',
    top: 10,
    left: 15,
  },
});

export {styles};
