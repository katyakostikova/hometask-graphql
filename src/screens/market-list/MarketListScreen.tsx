import {useLazyQuery} from '@apollo/client';
import React, {useCallback, useContext, useEffect} from 'react';
import {View, FlatList, Button} from 'react-native';
import FloatingButton from '../../components/market-list/floating-button/FloatingButton';
import ProductPreview from '../../components/market-list/product-preview/ProductPreview';
import SearchBar from '../../components/market-list/search-bar/SearchBar';
import {AppColors} from '../../config/app-colors';
import {GET_PRODUCTS} from '../../services/queries';
import {UserContext} from '../../store/context';
import Preloader from '../preloader/Preloader';
import {styles} from './styles';

const loadProductsFilter = {
  first: 50,
  after: null,
  filter: '',
};

const MarketListScreen = () => {
  const {setCurrentUser} = useContext(UserContext);
  const [getProducts, {data}] = useLazyQuery(GET_PRODUCTS);

  const loadMoreProducts = useCallback(() => {
    // loadProductsFilter.after = data.products.pageInfo.endCursor;
    // getProducts({
    //   variables: {
    //     first: loadProductsFilter.first,
    //     after: loadProductsFilter.after,
    //     filter: loadProductsFilter.filter,
    //   },
    // });
  }, []);

  const filterProducts = useCallback(
    (filterText: string) => {
      loadProductsFilter.after = null;
      loadProductsFilter.filter = filterText;
      getProducts({
        variables: {
          first: loadProductsFilter.first,
          after: loadProductsFilter.after,
          filter: loadProductsFilter.filter,
        },
      });
    },
    [getProducts],
  );

  const handleLogout = () => {
    setCurrentUser(undefined);
  };

  useEffect(() => {
    getProducts({
      variables: {
        first: loadProductsFilter.first,
        after: loadProductsFilter.after,
        filter: loadProductsFilter.filter,
      },
    });
  }, [getProducts]);

  if (!data) {
    return <Preloader />;
  }

  return (
    <View style={styles.screen}>
      <View style={styles.buttonLogout}>
        <Button
          title="Logout"
          color={AppColors.ERROR_TEXT}
          onPress={handleLogout}
        />
      </View>

      <SearchBar onSearch={filterProducts} />
      <View style={styles.productsList}>
        <FlatList
          onEndReachedThreshold={0.1}
          onEndReached={loadMoreProducts}
          data={data.products.nodes}
          refreshing={!data}
          onRefresh={() => filterProducts('')}
          showsVerticalScrollIndicator={false}
          renderItem={item => {
            return <ProductPreview product={item.item} />;
          }}
        />
      </View>
      <FloatingButton />
    </View>
  );
};

export default MarketListScreen;
