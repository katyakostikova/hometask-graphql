/* eslint-disable react-native/no-inline-styles */
import {useTheme} from '@react-navigation/native';
import React, {useContext, useEffect, useState} from 'react';
import {
  View,
  Button,
  TouchableOpacity,
  Text,
  TouchableWithoutFeedback,
  Keyboard,
  Alert,
} from 'react-native';
import TextInputApp from '../../components/text-input/TextInputAuth';
import {ButtonsOpacity} from '../../config/buttons-opacity';
import {
  validateEmail,
  validateNumber,
  validatePassword,
  validateUsername,
} from '../../helpers/strings/auth';
import Preloader from '../preloader/Preloader';
import {styles} from './styles';
import {useMutation} from '@apollo/client';
import {LOGIN, REGISTER} from '../../services/queries';
import {UserContext} from '../../store/context';

const AuthScreen = () => {
  const {colors} = useTheme();
  const [isRegister, setIsRegister] = useState(false);

  const [email, setEmail] = useState('');
  const [name, setName] = useState('');
  const [password, setPassword] = useState('');
  const [phoneNumber, setPhoneNumber] = useState('');

  const [handleLogin, {data, loading, error, reset}] = useMutation(LOGIN);
  const [handleRegister, {data: regData}] = useMutation(REGISTER);
  const {setCurrentUser} = useContext(UserContext);

  const handleChangeAuthMethod = () => {
    setEmail('');
    setName('');
    setPassword('');
    setPhoneNumber('');
    setIsRegister(state => !state);
  };

  const handleKeyboardDismiss = () => {
    Keyboard.dismiss();
  };

  const handleUserAuth = () => {
    const isValidEmail = validateEmail(email);
    const isValidPassword = validatePassword(password);
    if (!isValidEmail || !isValidPassword) {
      Alert.alert('Invalid credentials', 'Enter valid data');
      return;
    }
    if (!isRegister) {
      handleLogin({variables: {email, password}});
    } else {
      const isValidName = validateUsername(name);
      const isValidNumber = validateNumber(phoneNumber);
      if (!isValidName || !isValidNumber) {
        Alert.alert('Invalid credentials', 'Enter valid data');
        return;
      }
      handleRegister({variables: {email, name, phoneNumber, password}});
    }
  };
  useEffect(() => {
    if (data) {
      setCurrentUser(data.login);
    }
  }, [data, setCurrentUser]);

  if (loading) {
    return <Preloader />;
  }

  if (error) {
    Alert.alert('Invalid credentials', 'Email or password is incorrect');
    reset();
  }

  if (regData && isRegister) {
    Alert.alert(
      'Created account',
      'Account has been created successfully, please log in',
    );
  }

  return (
    <TouchableWithoutFeedback onPress={handleKeyboardDismiss}>
      <View style={styles.screen}>
        <View
          style={{
            ...styles.formContainer,
            backgroundColor: colors.card,
            height: isRegister ? '80%' : '60%',
          }}>
          <TextInputApp
            value={email}
            keyboardType="email-address"
            onChange={setEmail}
            placeholder="Enter email"
          />
          {isRegister && (
            <>
              <TextInputApp
                value={name}
                onChange={setName}
                placeholder="Enter name"
              />
              <TextInputApp
                value={phoneNumber}
                onChange={setPhoneNumber}
                keyboardType="phone-pad"
                placeholder="Enter phone number"
              />
            </>
          )}
          <TextInputApp
            value={password}
            onChange={setPassword}
            isPassword={true}
            placeholder="Enter password"
          />
          <View style={styles.button}>
            <Button
              onPress={handleUserAuth}
              color={colors.primary}
              title={isRegister ? 'Register' : 'Login'}
            />
          </View>
          <TouchableOpacity
            activeOpacity={ButtonsOpacity.LINKS}
            onPress={handleChangeAuthMethod}>
            <Text style={{...styles.testLink, color: colors.primary}}>
              {isRegister ? 'Back to Login' : 'Register'}
            </Text>
          </TouchableOpacity>
        </View>
      </View>
    </TouchableWithoutFeedback>
  );
};

export default AuthScreen;
