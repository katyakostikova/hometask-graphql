import {Asset} from 'react-native-image-picker';

export interface Register {
  email: string;
  password: string;
  name: string;
  phoneNumber: string;
  avatar?: Asset;
}
