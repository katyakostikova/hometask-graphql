export interface AddProduct {
  title: string;
  price: string;
  description: string;
}
