export interface Filters {
  page: number;
  perPage: number;
  filter?: string;
}
