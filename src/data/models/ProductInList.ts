import {Picture} from './Picture';

export interface ProductInList {
  id: number;
  title: string;
  price: number;
  description: string;
  pictures: Picture[];
}
