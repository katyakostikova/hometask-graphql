import React, {createContext, ReactNode, useState} from 'react';
import {User} from '../data/models/User';

interface ContextState {
  currentUser: User | undefined;
  setCurrentUser: React.Dispatch<React.SetStateAction<User | undefined>>;
}

const UserContext = createContext({} as ContextState);

interface Props {
  children: ReactNode;
}

const UserProvider = ({children}: Props) => {
  const [currentUser, setCurrentUser] = useState<User | undefined>(undefined);

  const contextValue = {currentUser, setCurrentUser};

  return (
    <UserContext.Provider value={contextValue}>{children}</UserContext.Provider>
  );
};

export {UserContext, UserProvider};
