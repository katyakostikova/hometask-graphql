const IconSizes = {
  DEFAULT_ICON: 30,
  GALLERY_ICON: 35,
  CALL_ICON: 25,
  DELETE_IMAGE_ICON: 30,
};

export {IconSizes};
