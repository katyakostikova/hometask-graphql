const IconNames = {
  SEARCH: 'search',
  BACK: 'arrow-back',
  FORWARD: 'arrow-forward',
  CALL: 'call',
  ADD: 'add',
  DELETE: 'trash-bin',
};

export {IconNames};
