import {NativeStackNavigationProp} from '@react-navigation/native-stack';

export type RootStackParamList = {
  MarketList: undefined;
  ProductDetails: {
    productId: number;
  };
  AddProduct: undefined;
  Auth: undefined;
};

export type MarketListScreenProp = NativeStackNavigationProp<
  RootStackParamList,
  'MarketList'
>;

export type ProductDetailsScreenProp = NativeStackNavigationProp<
  RootStackParamList,
  'ProductDetails'
>;

export type AddProductScreenProp = NativeStackNavigationProp<
  RootStackParamList,
  'AddProduct'
>;

export type ProductDetailsScreenRouteProp = {
  params: {
    productId: number;
  };
};
