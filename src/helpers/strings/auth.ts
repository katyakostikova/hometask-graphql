const validateUsername = (name: string) => {
  if (name.trim().length < 3) {
    return false;
  }
  return true;
};

const validateEmail = (email: string) => {
  const isValid = email
    .toLowerCase()
    .match(
      /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/,
    );
  return isValid;
};

const validatePassword = (password: string) => {
  if (password.trim().length < 3) {
    return false;
  }
  return true;
};

const validateNumber = (number: string) => {
  const isValid = number
    .toLowerCase()
    .match(/^[\+]?[(]?[0-9]{3}[)]?[-\s\.]?[0-9]{3}[-\s\.]?[0-9]{4,6}$/im);
  return isValid;
};

export {validateUsername, validateEmail, validatePassword, validateNumber};
