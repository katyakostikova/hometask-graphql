const cropPreviewText = (text: string, number: number) => text.slice(0, number);

export {cropPreviewText};
