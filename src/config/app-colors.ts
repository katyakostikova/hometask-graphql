const AppColors = {
  BACKGROUND_DARK: '#333436',
  BACKGROUND_LIGHT: '#a8c7a5',
  TEXT_LIGHT: '#000000',
  TEXT_DARK: '#ffffff',
  PLACEHOLDER_LIGHT: '#636362',
  PLACEHOLDER_DARK: '#deddd9',
  PRIMARY_LIGHT: '#103d07',
  PRIMARY_DARK: '#25ab16',
  CARD_LIGHT: '#ffffff',
  CARD_DARK: '#18191a',
  BUTTON_DISABLED: '#8c8c8c',
  CALL_BUTTON: '#ffffff',
  ERROR_TEXT: '#b01010',
};

export {AppColors};
