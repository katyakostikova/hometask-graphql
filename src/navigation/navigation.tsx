import {NavigationContainer} from '@react-navigation/native';
import {createNativeStackNavigator} from '@react-navigation/native-stack';
import React, {useContext} from 'react';
import {useColorScheme} from 'react-native';
import {RootStackParamList} from '../common/types/navigation';
import {
  NavigationThemeDark,
  NavigationThemeLight,
} from '../config/navigation-theme';
import AddProductScreen from '../screens/add-product/AddProductScreen';
import AuthScreen from '../screens/auth/AuthScreen';
import MarketListScreen from '../screens/market-list/MarketListScreen';
import ProductDetailsScreen from '../screens/product-details/ProductDetailsScreen';
import {UserContext} from '../store/context';

const Stack = createNativeStackNavigator<RootStackParamList>();

const screenOptions = {
  headerShown: false,
};

const RootNavigator: React.FC = () => {
  const colorScheme = useColorScheme();
  const {currentUser} = useContext(UserContext);

  return (
    <NavigationContainer
      theme={
        colorScheme === 'dark' ? NavigationThemeDark : NavigationThemeLight
      }>
      <Stack.Navigator initialRouteName="Auth" screenOptions={screenOptions}>
        {!currentUser ? (
          <Stack.Screen name="Auth" component={AuthScreen} />
        ) : (
          <>
            <Stack.Screen name="MarketList" component={MarketListScreen} />
            <Stack.Screen
              name="ProductDetails"
              component={ProductDetailsScreen}
            />
            <Stack.Screen name="AddProduct" component={AddProductScreen} />
          </>
        )}
      </Stack.Navigator>
    </NavigationContainer>
  );
};

export default RootNavigator;
