import {useTheme} from '@react-navigation/native';
import React from 'react';
import {View, TextInput, KeyboardTypeOptions} from 'react-native';
import {styles} from './styles';

interface TextInputAuthProps {
  placeholder: string;
  value: string;
  keyboardType?: KeyboardTypeOptions;
  isPassword?: boolean;
  multiline?: boolean;
  numberOfLines?: number;
  onChange: React.Dispatch<React.SetStateAction<string>>;
}

const TextInputApp = ({
  placeholder,
  value,
  keyboardType,
  isPassword,
  multiline,
  numberOfLines,
  onChange,
}: TextInputAuthProps) => {
  const {colors} = useTheme();
  return (
    <View style={styles.inputContainer}>
      <View
        style={{
          ...styles.border,
          borderColor: colors.border,
        }}>
        <TextInput
          style={{
            ...styles.input,
            color: colors.text,
          }}
          placeholderTextColor={colors.border}
          placeholder={placeholder}
          secureTextEntry={isPassword}
          multiline={multiline}
          numberOfLines={numberOfLines}
          keyboardType={keyboardType ? keyboardType : 'default'}
          value={value}
          onChangeText={text => onChange(text)}
        />
      </View>
    </View>
  );
};

export default TextInputApp;
