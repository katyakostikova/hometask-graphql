import {StyleSheet} from 'react-native';

const styles = StyleSheet.create({
  button: {
    width: 50,
    height: 50,
    borderRadius: 25,
    justifyContent: 'center',
    alignItems: 'center',
    elevation: 10,
    position: 'absolute',
    bottom: 30,
    right: 30,
  },
});

export {styles};
