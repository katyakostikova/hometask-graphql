import {useNavigation, useTheme} from '@react-navigation/native';
import React from 'react';
import {TouchableOpacity} from 'react-native';
import Icon from 'react-native-vector-icons/Ionicons';
import {IconNames} from '../../../common/enums/components/icon-names';
import {IconSizes} from '../../../common/enums/components/icon-sizes';
import {AddProductScreenProp} from '../../../common/types/navigation';
import {AppColors} from '../../../config/app-colors';
import {ButtonsOpacity} from '../../../config/buttons-opacity';
import {styles} from './styles';

// interface FloatingButtonProps {}

const FloatingButton = () => {
  const {colors} = useTheme();
  const navigation = useNavigation<AddProductScreenProp>();

  const moveToAddProduct = () => {
    navigation.navigate('AddProduct');
  };
  return (
    <TouchableOpacity
      onPress={moveToAddProduct}
      style={{...styles.button, backgroundColor: colors.primary}}
      activeOpacity={ButtonsOpacity.FLOATING_BUTTON}>
      <Icon
        name={IconNames.ADD}
        size={IconSizes.GALLERY_ICON}
        color={AppColors.CALL_BUTTON}
      />
    </TouchableOpacity>
  );
};

export default FloatingButton;
