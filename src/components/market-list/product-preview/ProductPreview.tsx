import {useNavigation, useTheme} from '@react-navigation/native';
import React from 'react';
import {View, Text, Image, TouchableOpacity, Dimensions} from 'react-native';
import {NoPictureUrl} from '../../../common/assets/no-picture';
import {ProductDetailsScreenProp} from '../../../common/types/navigation';
import {ButtonsOpacity} from '../../../config/buttons-opacity';
import {ProductInList} from '../../../data/models/ProductInList';
import {cropPreviewText} from '../../../helpers/strings/product-preview';
import {styles} from './styles';

interface ProductPreviewProps {
  product: ProductInList;
}

const ProductPreview = ({product}: ProductPreviewProps) => {
  const dimensions = Dimensions.get('window');
  const {colors} = useTheme();
  const navigation = useNavigation<ProductDetailsScreenProp>();

  const handleProductDetails = () => {
    navigation.navigate('ProductDetails', {
      productId: product.id,
    });
  };

  return (
    <View
      style={{
        ...styles.card,
        width: dimensions.width - 50,
        backgroundColor: colors.card,
      }}>
      <TouchableOpacity
        style={styles.container}
        onPress={handleProductDetails}
        activeOpacity={ButtonsOpacity.PRODUCT_CARD}>
        <View style={styles.imageContainer}>
          <Image
            style={styles.image}
            source={{
              uri:
                product.pictures?.length > 0
                  ? product.pictures[0].url
                  : NoPictureUrl,
            }}
          />
        </View>
        <View style={styles.productInfo}>
          <View style={styles.productHeader}>
            <Text style={{...styles.headerText, color: colors.text}}>
              {cropPreviewText(product.title.trim(), 15)}
              {product.title.length > 15 && '...'}
            </Text>
            <Text style={{color: colors.text}}>
              ${product.price.toFixed(2)}
            </Text>
          </View>
          <View style={styles.textDescriptionContainer}>
            <Text style={{color: colors.border}}>
              {cropPreviewText(product.description.trim(), 30)}...
            </Text>
          </View>
        </View>
      </TouchableOpacity>
    </View>
  );
};

export default ProductPreview;
