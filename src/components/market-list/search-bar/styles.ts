import {StyleSheet} from 'react-native';
import {TextSize} from '../../../common/enums/components/text-size';

const styles = StyleSheet.create({
  content: {
    padding: 20,
    paddingTop: 10,
  },
  searchBarContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-around',
    borderWidth: 1,
    borderRadius: 10,
  },
  searchInput: {
    width: '80%',
    fontSize: TextSize.MAIN,
  },
});

export {styles};
