import {useTheme} from '@react-navigation/native';
import React, {useState} from 'react';
import {View, TextInput, TouchableOpacity} from 'react-native';
import Icon from 'react-native-vector-icons/Ionicons';
import {IconNames} from '../../../common/enums/components/icon-names';
import {IconSizes} from '../../../common/enums/components/icon-sizes';
import {ButtonsOpacity} from '../../../config/buttons-opacity';
import {styles} from './styles';

interface SearchBarProps {
  onSearch: (filterText: string) => void;
}

const SearchBar = ({onSearch}: SearchBarProps) => {
  const {colors} = useTheme();
  const [searchText, setSearchText] = useState('');

  const handleSearch = () => {
    onSearch(searchText);
    setSearchText('');
  };

  return (
    <View style={styles.content}>
      <View
        style={{
          ...styles.searchBarContainer,
          backgroundColor: colors.card,
          borderColor: colors.border,
        }}>
        <TextInput
          style={{...styles.searchInput, color: colors.text}}
          placeholder="Search"
          value={searchText}
          onChangeText={setSearchText}
          placeholderTextColor={colors.border}
        />
        <TouchableOpacity
          activeOpacity={ButtonsOpacity.ICONS}
          onPress={handleSearch}>
          <Icon
            name={IconNames.SEARCH}
            size={IconSizes.DEFAULT_ICON}
            color={colors.primary}
          />
        </TouchableOpacity>
      </View>
    </View>
  );
};

export default SearchBar;
