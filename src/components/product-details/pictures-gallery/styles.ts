import {StyleSheet} from 'react-native';

const styles = StyleSheet.create({
  content: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
    width: '100%',
    padding: 30,
  },
  button: {
    borderRadius: 5,
  },
  imageContainer: {
    width: '85%',
    height: '100%',
    marginHorizontal: 10,
    alignItems: 'center',
    justifyContent: 'center',
    borderRadius: 10,
  },
  image: {
    width: '95%',
    height: '95%',
  },
});

export {styles};
