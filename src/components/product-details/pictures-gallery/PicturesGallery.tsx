import {useTheme} from '@react-navigation/native';
import React, {useState} from 'react';
import {Dimensions, Image, TouchableOpacity, View} from 'react-native';
import Icon from 'react-native-vector-icons/Ionicons';
import {NoPictureUrl} from '../../../common/assets/no-picture';
import {IconNames} from '../../../common/enums/components/icon-names';
import {IconSizes} from '../../../common/enums/components/icon-sizes';
import {AppColors} from '../../../config/app-colors';
import {ButtonsOpacity} from '../../../config/buttons-opacity';
import {Picture} from '../../../data/models/Picture';
import {styles} from './styles';

interface PicturesGalleryProps {
  pictures: Picture[] | undefined;
}

const PicturesGallery = ({pictures}: PicturesGalleryProps) => {
  const {colors} = useTheme();
  const [currentPicId, setCurrentPicId] = useState(0);
  const dimensions = Dimensions.get('window');

  const handlePrevPic = () => {
    setCurrentPicId(currId => (currId -= 1));
  };

  const handleNextPic = () => {
    setCurrentPicId(currId => (currId += 1));
  };

  return (
    <View style={{...styles.content, height: dimensions.height / 2}}>
      <TouchableOpacity
        style={{...styles.button, backgroundColor: colors.card}}
        disabled={currentPicId === 0 ? true : false}
        activeOpacity={ButtonsOpacity.ICONS}
        onPress={handlePrevPic}>
        <Icon
          name={IconNames.BACK}
          size={IconSizes.GALLERY_ICON}
          color={
            currentPicId === 0 ? AppColors.BUTTON_DISABLED : colors.primary
          }
        />
      </TouchableOpacity>
      <View
        style={{
          ...styles.imageContainer,
          backgroundColor: colors.card,
        }}>
        <Image
          style={styles.image}
          source={{
            uri:
              pictures && pictures.length > 0
                ? pictures[currentPicId].url
                : NoPictureUrl,
          }}
        />
      </View>
      <TouchableOpacity
        style={{...styles.button, backgroundColor: colors.card}}
        disabled={
          pictures && currentPicId === pictures.length - 1 ? true : false
        }
        activeOpacity={ButtonsOpacity.ICONS}
        onPress={handleNextPic}>
        <Icon
          name={IconNames.FORWARD}
          size={IconSizes.GALLERY_ICON}
          color={
            pictures &&
            (currentPicId === pictures.length - 1 || pictures.length === 0)
              ? AppColors.BUTTON_DISABLED
              : colors.primary
          }
        />
      </TouchableOpacity>
    </View>
  );
};

export default PicturesGallery;
