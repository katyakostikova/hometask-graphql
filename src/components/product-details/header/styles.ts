import {StyleSheet} from 'react-native';
import {TextSize} from '../../../common/enums/components/text-size';

const styles = StyleSheet.create({
  header: {
    flexDirection: 'row',
    alignItems: 'center',
    width: '100%',
    height: '7%',
    paddingHorizontal: 10,
  },
  text: {
    fontSize: TextSize.MAIN,
    fontWeight: 'bold',
    marginLeft: 10,
  },
});

export {styles};
