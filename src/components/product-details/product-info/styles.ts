import {StyleSheet} from 'react-native';
import {TextSize} from '../../../common/enums/components/text-size';

const styles = StyleSheet.create({
  content: {
    marginHorizontal: 20,
    marginBottom: 10,
    padding: 5,
    borderRadius: 10,
  },
  header: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    marginBottom: 10,
  },
  headerText: {
    fontSize: TextSize.TITLE,
    fontWeight: 'bold',
  },
  description: {
    fontSize: TextSize.MAIN,
  },
});

export {styles};
