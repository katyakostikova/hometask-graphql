import {useTheme} from '@react-navigation/native';
import React from 'react';
import {View, Text, Image, Dimensions} from 'react-native';
import {NoPictureUrl} from '../../../common/assets/no-picture';
import {Seller} from '../../../data/models/Seller';
import CallButton from './call-button/CallButton';
import {styles} from './styles';

interface SellerInfoProps {
  seller?: Seller;
}

const SellerInfo = ({seller}: SellerInfoProps) => {
  const dimensions = Dimensions.get('window');
  const {colors} = useTheme();
  return (
    <View
      style={{
        ...styles.content,
        height: dimensions.height / 4,
        backgroundColor: colors.card,
      }}>
      <View style={styles.sellerInfoContainer}>
        <View style={styles.imageContainer}>
          <Image
            style={styles.avatar}
            source={{uri: seller?.avatar ? seller.avatar : NoPictureUrl}}
          />
        </View>
        <View style={styles.sellerInfo}>
          <Text style={{...styles.sellerName, color: colors.text}}>
            {seller?.name}
          </Text>
          <Text style={{...styles.sellerNumber, color: colors.text}}>
            {seller?.phoneNumber}
          </Text>
        </View>
      </View>
      <View style={styles.buttonContainer}>
        <CallButton number={seller?.phoneNumber} />
      </View>
    </View>
  );
};

export default SellerInfo;
