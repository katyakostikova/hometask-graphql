import {useTheme} from '@react-navigation/native';
import React from 'react';
import {Linking, Text, TouchableOpacity, View} from 'react-native';
import Icon from 'react-native-vector-icons/Ionicons';
import {IconNames} from '../../../../common/enums/components/icon-names';
import {IconSizes} from '../../../../common/enums/components/icon-sizes';
import {AppColors} from '../../../../config/app-colors';
import {ButtonsOpacity} from '../../../../config/buttons-opacity';
import {styles} from './styles';

interface CallButtonProps {
  number?: string;
}

const CallButton = ({number}: CallButtonProps) => {
  const {colors} = useTheme();

  const handleCall = () => {
    Linking.openURL(`tel:${number}`);
  };

  return (
    <TouchableOpacity
      onPress={handleCall}
      style={{...styles.button, backgroundColor: colors.primary}}
      activeOpacity={ButtonsOpacity.PRODUCT_CARD}>
      <Text style={styles.text}>Call seller</Text>
      <View style={styles.iconContainer}>
        <Icon
          name={IconNames.CALL}
          size={IconSizes.CALL_ICON}
          color={AppColors.CALL_BUTTON}
        />
      </View>
    </TouchableOpacity>
  );
};

export default CallButton;
