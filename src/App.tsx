import React from 'react';
import {ApolloClient, ApolloProvider, InMemoryCache} from '@apollo/client';

import RootNavigator from './navigation/navigation';
import {UserProvider} from './store/context';

const client = new ApolloClient({
  uri: 'https://bsa-marketplace-graphql.azurewebsites.net/../graphql',
  cache: new InMemoryCache(),
});

const App = () => {
  return (
    <ApolloProvider client={client}>
      <UserProvider>
        <RootNavigator />
      </UserProvider>
    </ApolloProvider>
  );
};

export default App;
